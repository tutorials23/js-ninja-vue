import { mount } from "@cypress/vue";
import _App from "@/App.vue";

describe("HelloWorld", () => {
  it("renders a message", () => {
    mount(_App);

    cy.get("#wallet").type("Example");
    cy.get("#add-button").click();
    cy.get("#wallet").type("Example");
    cy.get("#add-button").click();
    cy.get(".ticker-block").should(($el) => {
      expect($el).to.have.length(3);
    });
  });
});
